FROM node
MAINTAINER Silvio Incalza (silvio@incalza.me)
LABEL org.label-schema.vcs-url="https://bitbucket.org/incalzame/incalzame-resume.git"
LABEL version="latest"

ENV HTML_DIR="/usr/share/nginx/html"
ENV IMAGES_DIR=$HTML_DIR/images
ENV TEMPLATE_GIT="https://bitbucket.org/incalzame/jsonresume-theme-incalzame.git"
ENV OUTPUT_TEMPLATE=incalzame
ENV RESUME_FILE="resume.json"
ENV RESUME_JSON_URL="https://bitbucket.org/incalzame/incalzame-resume/raw/master/resume.json"
ENV EMAIL="silvio@incalza.me"
ENV ADDRESS=""
ENV PHONE=""

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -yq install \
    curl wkhtmltopdf nginx && \
    rm -rf /var/lib/apt/lists/* && \
    npm install hackmyresume@">=1.3.0 <2" -g && \
    mkdir -p $IMAGES_DIR

COPY $RESUME_FILE $HTML_DIR/
COPY assets/images/*.jpeg $IMAGES_DIR/
COPY assets/images/*.png $IMAGES_DIR/

COPY start.sh /start.sh
RUN chmod +x /start.sh

VOLUME $HTML_DIR/
WORKDIR $HTML_DIR

CMD /start.sh

COPY nginx-start.sh /nginx-start.sh
RUN chmod +x /nginx-start.sh
COPY test.sh /test.sh
RUN chmod +x /test.sh

CMD /nginx-start.sh
EXPOSE 80 443
